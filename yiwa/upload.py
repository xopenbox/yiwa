# coding: utf-8

"""上传文件，通用方法"""

import base64
from mimetypes import guess_all_extensions, guess_type
import os
import uuid
from werkzeug.datastructures import FileStorage
from yiwa.settings import BASE_DIR
import urllib.parse


class FileUploader(object):
    """文件上传器"""

    def image(self, dir_path: str, image_data: str):
        """上传图片"""
        dir_abs_path = os.path.join(BASE_DIR, "apps", dir_path)
        if not os.path.exists(dir_abs_path):
            return False
        # 本地文件
        try:
            # url中文转义，避免找不到含有中文路径的图片
            image_data = urllib.parse.unquote(image_data)
            image_name = image_data.split("/")[-1]
            if os.path.exists(os.path.join(dir_abs_path, image_name)):
                return image_name
        except Exception as ex:
            pass
        # 异地上传
        try:
            # 图片文件扩展名
            file_ext = ".jpg"   # 默认扩展名
            image_type = guess_type(image_data)[0]
            image_all_ext = guess_all_extensions(image_type)
            if image_all_ext:
                file_ext = image_all_ext[-1]
            # 新的uuid文件名
            image_new_name = f"{str(uuid.uuid1())}{file_ext}"
            # 替换并提取base64数据
            base64_string = image_data.replace(f"data:{image_type};base64,", "")
            image_base64_data = base64.b64decode(base64_string)
            full_path = os.path.join(dir_abs_path, image_new_name)
            with open(full_path, 'wb') as file:
                file.write(image_base64_data)
            return image_new_name
        except Exception as ex:
            return False
