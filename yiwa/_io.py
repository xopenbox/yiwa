# coding: utf8

"""文件读取"""

import os
from importlib import import_module, reload
from yiwa.settings import BASE_DIR


def scan_apps():
    """扫描所有app"""
    apps_path = os.path.join(BASE_DIR, "apps")
    top_dirnames = next(os.walk(apps_path))[1]
    modules = [m for m in top_dirnames if not m.startswith("__")]

    _apps, _commands = [], []
    for module in modules:
        try:
            module_path = f"apps.{module}.configs"
            configs = import_module(module_path)
            reload(configs)
            print(f"成功加载插件配置：{module_path}")
            _apps.append((configs.APPID, configs.APPNAME))
            for name, command in configs.COMMANDS.items():
                _commands.append((name,
                                  ",".join(command.get("commands")),
                                  command.get("action"),
                                  command.get("global", 0),  # 默认非全局目录
                                  configs.APPID))
        except ImportError as ierror:
            print("加载插件配置失败或不存在", ierror)

    return (_apps, _commands)


if __name__ == "__main__":
    scan_apps()
