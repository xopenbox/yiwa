# coding: utf8

""""""

import platform
from importlib import import_module

try:
    systems = {"Darwin": "osx", "Linux": "rpi"}
    system = platform.system()
    snowboydetect = import_module(f"snowboy.{systems.get(system)}.snowboydetect")
except Exception as e:
    print(e)
